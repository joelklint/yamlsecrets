package lib

import (
	"errors"
	"fmt"
	"strconv"
	"strings"

	"gopkg.in/yaml.v2"
)

func ConvertTree(tree yaml.MapSlice, path []string, sourceSuffix string, resultSuffix string, encryptionKey []byte, encrypt bool) (yaml.MapSlice, []string, error) {
	result := yaml.MapSlice{}
	alteredPaths := []string{}
	var err error
	var value interface{}

	for _, element := range tree {
		sourceKey := element.Key.(string)
		thisSourcePath := append(path, sourceKey)
		resultKey := sourceKey

		switch value_parsed := element.Value.(type) {
		case yaml.MapSlice:
			if len(sourceKey) > len(sourceSuffix) && sourceKey[len(sourceKey)-len(sourceSuffix):] == sourceSuffix { // Should be encrypted
				return nil, nil, errors.New("YAML dictionaries are not supported")
			}
			value, alteredPaths, err = ConvertTree(value_parsed, thisSourcePath, sourceSuffix, resultSuffix, encryptionKey, encrypt)
			if err != nil {
				return nil, nil, err
			}
		default:
			if len(sourceKey) > len(sourceSuffix) && sourceKey[len(sourceKey)-len(sourceSuffix):] == sourceSuffix { // Should be encrypted
				valueString := fmt.Sprintf("%v", value_parsed)
				if encrypt {
					varType := fmt.Sprintf("%T", value_parsed)
					encryptedString, err := Encrypt(valueString, encryptionKey)
					if err != nil {
						return nil, nil, err
					}
					value = varType + "$" + encryptedString
				} else {
					splitted := strings.Split(valueString, "$")
					varType := splitted[0]
					decryptedString, err := Decrypt(splitted[1], encryptionKey)
					if err != nil {
						return nil, nil, err
					}
					switch varType {
					case "int":
						value, err = strconv.Atoi(decryptedString)
						if err != nil {
							return nil, nil, err
						}
					case "float64":
						value, err = strconv.ParseFloat(decryptedString, 64)
						if err != nil {
							return nil, nil, err
						}
					case "bool":
						value, err = strconv.ParseBool(decryptedString)
						if err != nil {
							return nil, nil, err
						}
					case "string":
						value = decryptedString
					default:
						return nil, nil, errors.New(fmt.Sprintf("Decryption of type %v is not supported", varType))
					}
				}
				if err != nil {
					return nil, nil, err
				}
				resultKey = sourceKey[:len(sourceKey)-len(sourceSuffix)] + resultSuffix
				alteredPaths = append(alteredPaths, strings.Join(append(path, resultKey), "."))
			} else {
				value = value_parsed
			}
		}

		result = append(result, yaml.MapItem{Key: resultKey, Value: value})
	}

	return result, alteredPaths, nil
}
