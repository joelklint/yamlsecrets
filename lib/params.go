package lib

import (
	"errors"
	"os"
	"path/filepath"
)

func GetBasePathByVersion(filename string, version string) (string, error) {
	if filename[len(filename)-5:] != ".yaml" {
		return "", errors.New("Must specify a yaml file")
	}

	if version != "" {
		filename = filename[:len(filename)-5-len(version)-1] + filename[len(filename)-5:]
	}

	return filename, nil
}

func GetBasePathWithVersion(basepath string, version string) (string, error) {
	if basepath[len(basepath)-5:] != ".yaml" {
		return "", errors.New("Must specify a yaml file")
	}

	if version != "" {
		basepath = basepath[:len(basepath)-5] + "." + version + basepath[len(basepath)-5:]
	}

	return basepath, nil
}

func MakeAbsolutePath(path string) (string, error) {
	fullPath, err := filepath.Abs(path)
	if err != nil {
		return "", err
	}
	return fullPath, nil
}

func CheckFileExists(path string) error {
	if _, err := os.Stat(path); os.IsNotExist(err) {
		return err
	}
	return nil
}

func GetKey(key string) ([]byte, error) {
	if len(key) == 0 {
		key = os.Getenv("YAMLSECRETS_KEY")
	}

	key_decoded := []byte(key)

	len_key := len(key_decoded)
	if len_key != 32 && len_key != 24 && len_key != 16 {
		return nil, errors.New("key must be 16, 24 or 32 bytes")
	}

	return key_decoded, nil
}
