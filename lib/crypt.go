package lib

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"encoding/base64"
	"io"
)

func Encrypt(cleartext string, key []byte) (string, error) {
	c, err := aes.NewCipher(key)
	if err != nil {
		return "", err
	}

	gcm, err := cipher.NewGCM(c)
	if err != nil {
		return "", err
	}

	nonce := make([]byte, gcm.NonceSize())
	_, err = io.ReadFull(rand.Reader, nonce)
	if err != nil {
		return "", err
	}

	encrypted := gcm.Seal(nonce, nonce, []byte(cleartext), nil)
	return base64.StdEncoding.EncodeToString(encrypted), nil
}

func Decrypt(ciphertext string, key []byte) (string, error) {
	c, err := aes.NewCipher(key)
	if err != nil {
		return "", err
	}

	gcm, err := cipher.NewGCM(c)
	if err != nil {
		return "", err
	}

	nonceSize := gcm.NonceSize()
	if len(ciphertext) < nonceSize {
		return "", err
	}

	cipher_bytes, err := base64.StdEncoding.DecodeString(ciphertext)
	if err != nil {
		return "", err
	}

	nonce, cipher_bytes := cipher_bytes[:nonceSize], cipher_bytes[nonceSize:]
	plaintext, err := gcm.Open(nil, nonce, []byte(cipher_bytes), nil)
	if err != nil {
		return "", err
	}

	return string(plaintext), nil
}
