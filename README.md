yamlsecrets is a tool for encrypting selective contents of yaml files

# Install
```sh
go install gitlab.com/joelklint/yamlsecrets@latest
```